/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pipex.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/21 12:27:36 by rduclos           #+#    #+#             */
/*   Updated: 2015/02/05 16:06:39 by rduclos          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell.h"

void		set_cmds(t_cmds *cmds)
{
	t_cmds	*tmp;

	ft_init_cmds(cmds);
	tmp = cmds;
	while (tmp->after != NULL)
		tmp = tmp->after;
	if (ft_verify_pipe(tmp) == 1)
	{
		while (ft_strcmp(tmp->str, "|") != 0)
			tmp = tmp->befor;
		ft_my_pipe(tmp);
	}
	else
	{
		while (tmp->befor != NULL)
			tmp = tmp->befor;
		ft_make_my_dir(tmp);
	}
}

void		make_my_pipe_in(t_cmds *cmds, int tabfd[2])
{
	t_cmds	*tmp;

	tmp = cmds;
	tmp = tmp->befor;
	dup2(tabfd[1], 1);
	close(tabfd[0]);
	if (ft_verify_pipe(tmp) == 1)
	{
		while (ft_strcmp(tmp->str, "|") != 0)
			tmp = tmp->befor;
		ft_my_pipe2(tmp, tabfd);
	}
	else
	{
		tmp = ft_re_set(cmds);
		if (ft_verify_dir(tmp) == 1)
			ft_make_my_dir(tmp);
		else
		{
			tmp->done = 1;
			make_my_builtin(tmp->str);
		}
	}
}

void		make_my_pipe_out(int father, t_cmds *cmds, int tabfd[2])
{
	t_cmds	*tmp;
	int		status;

	(void)father;
	tmp = cmds;
	dup2(tabfd[0], 0);
	close(tabfd[1]);
	father = fork();
	save_exec(father, 1);
	if (father > 0)
		waitpid(father, &status, 0);
	if (father == 0)
	{
		tmp = tmp->after;
		if (ft_verify_dir(tmp) == 1)
			ft_make_my_dir(tmp);
		else
		{
			tmp->done = 1;
			make_my_builtin(tmp->str);
		}
		exit(-1);
	}
}

void		ft_my_pipe2(t_cmds *cmds, int tabfd2[2])
{
	pid_t	father;
	int		tabfd[2];

	tabfd[1] = 0;
	dup2(tabfd2[1], tabfd[1]);
	cmds->done = 1;
	cmds->type = 'p';
	pipe(tabfd);
	father = fork();
	save_exec(father, 1);
	if (father > 0)
		make_my_pipe_out(father, cmds, tabfd);
	if (father == 0)
	{
		make_my_pipe_in(cmds, tabfd);
		exit(0);
	}
}

void		ft_my_pipe(t_cmds *cmds)
{
	pid_t	father;
	int		tabfd[2];
	int		status;

	cmds->done = 1;
	cmds->type = 'p';
	father = fork();
	save_exec(father, 1);
	if (father > 0)
		waitpid(father, &status, 0);
	if (father == 0)
	{
		pipe(tabfd);
		if ((father = fork()) > 0)
			make_my_pipe_out(father, cmds, tabfd);
		save_exec(father, 1);
		if (father == 0)
		{
			make_my_pipe_in(cmds, tabfd);
			waitpid(father, &status, 0);
			exit(0);
		}
		waitpid(father, &status, 0);
		exit(0);
	}
}

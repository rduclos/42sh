/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_my_cd3.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/13 12:21:04 by rduclos           #+#    #+#             */
/*   Updated: 2015/03/18 18:58:49 by rduclos          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell.h"

char	*make_cd_tilde(char *av)
{
	char	*av2;
	char	*str;

	if (av[0] == '~')
	{
		str = find_my_way("HOME=");
		av2 = ft_strjoin(str, av + 1);
		free(str);
	}
	else
		av2 = ft_strdup(av);
	return (av2);
}

char	*make_cd_1arg2(char *way, char **av)
{
	int		i;
	char	**tab;
	char	*tmp;

	i = 0;
	tab = ft_strsplit(av[0], '/');
	tmp = ft_strdup(way);
	while (tab[i])
	{
		if (ft_strcmp(tab[i], "..") == 0)
			tmp = cut_last_dir(tmp);
		else if (ft_strcmp(tab[i], ".") != 0)
			tmp = add_dir_way(tmp, tab[i]);
		i++;
	}
	ft_free_tab(tab);
	free(way);
	return (tmp);
}

#include <stdio.h>

char	*make_cd_2arg2(char *way, char **av)
{
	char	*tmp;
	char	*tmp2;
	char	*way2;
	int		i;

	i = ft_strlen(av[0]) - ft_strlen(av[1]);
	way2 = (char *)malloc(sizeof(char) * (ft_strlen(way) - i + 1));
	way2 = ft_strcpy(way2, way);
	tmp = ft_strstr(way2, av[0]);
	if (tmp == NULL)
		return (NULL);
	if ((tmp2 = ft_strchr(tmp, '/')) != NULL)
		tmp2 = ft_strdup(tmp2);
	tmp = ft_strcpy(tmp, av[1]);
	if (tmp2 != NULL)
	{
		ft_strcpy(tmp + ft_strlen(av[1]), tmp2);
		free(tmp2);
	}
	free(way);
	return (way2);
}

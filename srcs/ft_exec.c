/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 17:11:55 by rduclos           #+#    #+#             */
/*   Updated: 2015/01/31 20:24:55 by rduclos          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell.h"

pid_t		save_exec(pid_t son, int sig)
{
	static pid_t	exec;

	if (sig == 1)
		exec = son;
	else if (sig == 0)
		return (exec);
	else
		exec = -2;
	return (son);
}

t_pidt		*save_exec2(t_pidt *son, int sig)
{
	static t_pidt	*exec;

	if (sig == 1)
		exec = son;
	else if (sig == 0)
		return (exec);
	else
		exec = NULL;
	return (son);
}

t_pidt		*add_lc_exec(t_pidt *lst, pid_t add)
{
	t_pidt		*new;

	new = (t_pidt *)malloc(sizeof(t_pidt));
	new->next = NULL;
	new->son = add;
	if (lst != NULL)
		new->next = lst;
	return (new);
}

void		del_lc_exec(t_pidt *lst)
{
	t_pidt		*tmp;

	while (lst != NULL)
	{
		tmp = lst;
		lst = lst->next;
		free(tmp);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verify_no_exist_alias.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/20 19:25:52 by rduclos           #+#    #+#             */
/*   Updated: 2015/01/20 19:25:58 by rduclos          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_minishell.h"

int			verify_no_exist_alias(char **alias)
{
	t_alias		*l_alias;

	l_alias = ft_my_sig_alias(NULL, 0);
	while (l_alias)
	{
		if ((ft_strcmp(l_alias->name, *alias) == 0)
			|| ft_strcmp("emacs", *alias) == 0)
			return (ALIAS_EXIST);
		l_alias = l_alias->next;
	}
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_structglob.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/20 19:53:52 by rduclos           #+#    #+#             */
/*   Updated: 2015/01/31 20:30:58 by rduclos          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRUCTGLOB_H
# define FT_STRUCTGLOB_H

typedef struct		s_alias
{
	char			*name;
	char			*cmd;
	struct s_alias	*next;
}					t_alias;

typedef struct		s_cmds
{
	char			*str;
	char			type;
	int				done;
	struct s_cmds	*after;
	struct s_cmds	*befor;
}					t_cmds;

typedef struct		s_char
{
	char			c;
	int				b_prompt;
	int				a_prompt;
	struct s_char	*after;
	struct s_char	*befor;
}					t_char;

typedef struct		s_pidt
{
	pid_t			son;
	struct s_pidt	*next;
}					t_pidt;

char				**g_my_env;
int					g_son;
t_pidt				*g_pipe;
t_pidt				*g_drct;

#endif
